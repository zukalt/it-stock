FROM node:lts-alpine

CMD mkdir -p /opt/app
COPY . /opt/app
WORKDIR /opt/app

RUN \ 
# npm install -g -s --no-progress yarn && \
    yarn install && \
    yarn run build --release && \
    yarn cache clean
#    yarn install --production --ignore-scripts --prefer-offline && \
#    echo yes | yarn  --cwd ./it-stock-web eject && \
#    yarn run prune && \

RUN adduser -D it-stock
USER it-stock

CMD [ "yarn", "start" ]

