const session = require(process.env.APP_SESSION_STORE_IMPL)(JSON.parse(process.env.APP_SESSION_CONFIG));
const auth = require(process.env.APP_AUTH_IMPL)(JSON.parse(process.env.APP_AUTH_CONFIG));
const ownerStore = require(process.env.APP_OWNERS_STORE)(JSON.parse(process.env.APP_OWNERS_STORE_CONFIG));
const assetTypeStore = require(process.env.APP_ASSET_TYPES_STORE)(JSON.parse(process.env.APP_ASSET_TYPES_STORE_CONFIG));
const assetTransferStore = require(process.env.APP_TRANSFERS_STORE)(JSON.parse(process.env.APP_TRANSFERS_STORE_CONFIG));


const appAuth = require('it-stock-core/app/auth')({
    session,
    ...auth
});

const appAssetTypes = require('it-stock-core/app/asset_types')({
    assetTypeStore,
    assetTransferStore
});
const appAssetOwners = require('it-stock-core/app/owners')({
    ownerStore,
    assetTransferStore
});

const appTransfers = require('it-stock-core/app/transfers')({
    ownerStore,
    assetTypeStore,
    assetTransferStore
});


const appInitialization = require('it-stock-core/app/initialization');
const initApp = appInitialization.bind(null, {ownerStore});


module.exports = {
    initApp,
    appAuth,
    appAssetTypes,
    appAssetOwners,
    appTransfers,
    session
};
