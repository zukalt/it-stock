const {StockAssetType, StockAssetOwner} = require('it-stock-core/model');

module.exports = ({Auth, Owners, AssetTypes, AssetTransfers,}) => {

    const bindTokenToEndpoint = (client, token) => {
        const boundClient = {};
        Object.keys(client).forEach(f => {
            boundClient[f] = client[f].bind(null, token);
        });
        return boundClient;
    };

    describe('Assets management (transfers)', () => {
        let token;
        let owners = ['A', 'B', 'C'].map(o => {
            return {
                id: `_id:${o}`,
                fullName: o,
                email: `${o}@example.com`
            }
        });
        let types = ['Cat1/Item1', 'Cat1/Item2', 'Cat2/Item1'].map(
            t => StockAssetType.from({category: t.split('/')[0], name: t.split('/')[1]})
        );

        beforeAll(async () => {
            let res = await Auth.adminLogin();
            expect(res.statusCode).toEqual(200);
            token = res.body.token;

            // register presets
            await Promise.all(owners.map(o => Owners.add(token, o)));
            await Promise.all(types.map(t => AssetTypes.add(token, t)));
        });


        describe('Asset Transfers', () => {

            let client;

            beforeAll(() => {
                client = bindTokenToEndpoint(AssetTransfers, token);
            });

            test('Transfer restricts owners and/or asset type from deletion', async () => {
                let response = await client.transfer({
                    fromOwner: owners[0].id,
                    toOwner: owners[1].id,
                    assetType: types[0].id,
                    condition: 'USED',
                    date: Date.now()
                });
                expect(response.statusCode).toEqual(200);
                expect(response.body).toMatchObject({
                    id: expect.stringMatching(/\w{8,}/)
                });
                const transfer = response.body;

                response = await Owners.remove(token, owners[0].id);
                expect(response.statusCode).toBe(422);

                response = await Owners.remove(token, owners[1].id);
                expect(response.statusCode).toBe(422);

                response = await AssetTypes.remove(token, types[0].id);
                expect(response.statusCode).toBe(422);

                // undo transfer and try remove again
                response = await client.undoTransfer(transfer.id);
                expect(response.statusCode).toBe(200);
                // test delete
                response = await Owners.remove(token, owners[0].id);
                expect(response.statusCode).toBe(200);
                await Owners.add(token, owners[0]); // put it back

                response = await Owners.remove(token, owners[1].id);
                expect(response.statusCode).toBe(200);
                await Owners.add(token, owners[1]); // put it back

                response = await AssetTypes.remove(token, types[0].id);
                expect(response.statusCode).toBe(200);
                await AssetTypes.add(token, types[0]); // get it back

            });

            test('Valid transfer should appear in log', async () => {
                let response = await client.transfer({
                    fromOwner: StockAssetOwner.SUPPLIER.id,
                    toOwner: StockAssetOwner.STOCK.id,
                    assetType: types[0].id,
                    condition: 'NEW',
                    date: Date.now()
                });
                expect(response.statusCode).toEqual(200);
                const transfer = response.body;

                response = await client.transfer({
                    fromOwner: owners[0].id,
                    toOwner: owners[1].id,
                    assetType: types[2].id,
                    condition: 'NEW',
                    date: Date.now()
                });
                expect(response.statusCode).toEqual(200);
                const transfer2 = response.body;

                const searchAndCheck = async (params) => {
                    response = await client.searchLog(params);
                    expect(response.statusCode).toEqual(200);
                    expect(response.body).toHaveLength(1);
                    expect(response.body[0]).toEqual(transfer);
                };

                await searchAndCheck({ownerId: StockAssetOwner.SUPPLIER.id});
                await searchAndCheck({ownerId: StockAssetOwner.STOCK.id});
                await searchAndCheck({assetTypeId: types[0].id});
                await searchAndCheck({assetTypeId: types[0].id, ownerId: StockAssetOwner.STOCK.id});
                await searchAndCheck({assetTypeId: types[0].id, ownerId: StockAssetOwner.SUPPLIER.id});

                await client.undoTransfer(transfer.id);
                await client.undoTransfer(transfer2.id);

            });

            test('Transfer can be done only for defined owners and types', async () => {

                let response = await client.transfer({
                    fromOwner: StockAssetOwner.STOCK.id,
                    toOwner: 'non-existing-owner-id',
                    assetType: types[0].id,
                    condition: 'NEW',
                    date: Date.now()
                });
                expect(response.statusCode).toBe(422); // toOwner is not defined

                response = await client.transfer({
                    fromOwner: 'non-existing-owner-id',
                    toOwner:  StockAssetOwner.STOCK.id,
                    assetType: types[0].id,
                    condition: 'NEW',
                    date: Date.now()
                });
                expect(response.statusCode).toBe(422); // fromOwner is not defined

                response = await client.transfer({
                    fromOwner: StockAssetOwner.SUPPLIER.id,
                    toOwner:  StockAssetOwner.STOCK.id,
                    assetType: 'not-existing-type-id',
                    condition: 'NEW',
                    date: Date.now()
                });
                expect(response.statusCode).toBe(422); // assetType is not defined

            });

            describe('Transfer flows restrictions', () => {

                test('Disposer can only accept transfers', async () => {

                    const sendAndCheck = async (toOwner) => {
                        let response = await client.transfer({
                            fromOwner: StockAssetOwner.DISPOSER.id,
                            toOwner:  toOwner,
                            assetType: types[0].id,
                            condition: 'NEW',
                            date: Date.now()
                        });
                        expect(response.statusCode).toBe(422);
                    } ;

                    await sendAndCheck(StockAssetOwner.STOCK.id);
                    await sendAndCheck(StockAssetOwner.ASSEMBLER.id);
                    await sendAndCheck(StockAssetOwner.SUPPLIER.id);
                    await sendAndCheck(StockAssetOwner.DISPOSER.id);
                    await sendAndCheck(owners[0].id);
                });

                test('SUPPLIER transfer only to STOCK', async () => {

                    const sendAndCheck = async (toOwner) => {
                        let response = await client.transfer({
                            fromOwner: StockAssetOwner.SUPPLIER.id,
                            toOwner:  toOwner,
                            assetType: types[0].id,
                            condition: 'NEW',
                            date: Date.now()
                        });
                        expect(response.statusCode).toBe(422);
                    } ;

                    await sendAndCheck(StockAssetOwner.DISPOSER.id);
                    await sendAndCheck(StockAssetOwner.ASSEMBLER.id);
                    await sendAndCheck(StockAssetOwner.SUPPLIER.id);
                    await sendAndCheck(owners[0].id);

                })

            });

            // it.skip('Stock Procurement', () => {
            // });
            // it.skip('Stock Disposal', () => {
            // });
            // it.skip('Stock Assembly', () => {
            // });
            // it.skip('Stock Transfer', () => {
            // });
            // it.skip('Stock Report 1', () => {
            // });
            // it.skip('Stock Report 2', () => {
            // });
        });

    });


};
