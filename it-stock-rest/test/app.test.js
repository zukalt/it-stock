require('dotenv-flow').config({
    default_node_env: 'test',
    path: `${__dirname}/..`
});
const APP_URL = process.argv.find(arg => arg.match(/https?:\/\//)) ;
const request = require('supertest');
const {app, initApp} = APP_URL ?  {app: APP_URL, initApp: () => console.log(`Testing url = ${APP_URL}`) } : require('../app');


const Auth = {
    login: (email, password) => {
        return request(app)
            .post('/api/auth/login')
            .send({
                email: email,
                password: password,
            });
    },

    logout: (token) => {
        return request(app)
            .post('/api/auth/logout')
            .set('Authorization', `Bearer ${token}`);
    },

    whoami: (token) => {
        return request(app)
            .get('/api/auth/whoami')
            .set('Authorization', `Bearer ${token}`);
    }
};

Auth.adminLogin = Auth.login.bind(null, 'admin@example.com', 'password');
Auth.userLogin = Auth.login.bind(null, 'user@example.com', 'password');
Auth.reportViewerLogin = Auth.login.bind(null, 'report-viewer@example.com', 'password');

const Owners = {
    get: (token, id) => {
        return request(app)
            .get(`/api/owners/${id}`)
            .set('Authorization', `Bearer ${token}`);
    },
    search: (token, {first, like}) => {
        return request(app)
            .get(`/api/owners/`)
            .query({first: first, like: like})
            .set('Authorization', `Bearer ${token}`);
    },
    add: (token, owner) => {
        return request(app)
            .post(`/api/owners/`)
            .set('Authorization', `Bearer ${token}`)
            .send(owner);
    },
    update: (token, id, owner) => {
        return request(app)
            .patch(`/api/owners/${id}`)
            .set('Authorization', `Bearer ${token}`)
            .send(owner);
    },
    remove: (token, id) => {
        return request(app)
            .delete(`/api/owners/${id}`)
            .set('Authorization', `Bearer ${token}`);
    }
};

const AssetTypes = {
    get: (token, id) => {
        return request(app)
            .get(`/api/assets/${id}`)
            .set('Authorization', `Bearer ${token}`);
    },
    search: (token, {first, like}) => {
        return request(app)
            .get(`/api/assets/`)
            .query({first: first, like: like})
            .set('Authorization', `Bearer ${token}`);
    },
    add: (token, assetType) => {
        return request(app)
            .post(`/api/assets/`)
            .set('Authorization', `Bearer ${token}`)
            .send(assetType);
    },
    update: (token, id, assetType) => {
        return request(app)
            .patch(`/api/assets/${id}`)
            .set('Authorization', `Bearer ${token}`)
            .send(assetType);
    },
    remove: (token, id) => {
        return request(app)
            .delete(`/api/assets/${id}`)
            .set('Authorization', `Bearer ${token}`);
    }
};

const AssetTransfers = {
    transfer: (token, transferRequest) => {
        return request(app)
            .post('/api/transfers')
            .set('Authorization', `Bearer ${token}`)
            .send(transferRequest);
    },

    undoTransfer: (token, id) => {
        return request(app)
            .delete(`/api/transfers/${id}`)
            .set('Authorization', `Bearer ${token}`);
    },

    searchLog: (token, {ownerId, assetTypeId}) => {
        return request(app)
            .get(`/api/transfers/`)
            .set('Authorization', `Bearer ${token}`)
            .query({ownerId, assetTypeId});
    },
};

describe('App general', () => {
    beforeAll(initApp);
    test("initialization should not fail if done multiple times", initApp);
    test('App system error format', async () => {
        let response = await request(app).get('/api/fail') ;
        expect(response.statusCode).toBe(500);
        expect(response.body).toEqual({"error": "error-format-check"});
    });

    test('Authorization header provided without "Bearer " prefix', async () => {
        let session = await Auth.reportViewerLogin();
        let response = await request(app)
            .get('/api/auth/whoami')
            .set('Authorization', session.body.token);
        expect(response.statusCode).toBe(200);
    });

    test('Authorization header not provided', async () => {
        let response = await request(app)
            .get('/api/auth/whoami');
        expect(response.statusCode).toBe(401);
    });

    test('Static files serving', async ()=> {
        let response = await request(app).get('/');
        expect(response.statusCode).toBe(200);
        expect(response.header['content-type']).toMatch(/^text\/html/);
        let response2 = await request(app).get('/anithing not star');
        expect(response.statusCode).toBe(200);
        expect(response.header['content-type']).toMatch(/^text\/html/);
        expect(response.body).toStrictEqual(response2.body);
    })
});


require('./app.test.auth')({Auth});
require('./app.test.owners')({Auth, Owners});
require('./app.test.asset-types')({Auth, AssetTypes});
require('./app.test.asset-transfers')({Auth, Owners, AssetTypes, AssetTransfers});

