module.exports = ({Auth, Owners}) => {

    describe('Reference Types Management  AssetOwners', () => {

        let session;

        beforeAll(async () => {
            let res = await Auth.adminLogin();
            expect(res.statusCode).toEqual(200);
            session = res.body;
        });

        it('should have 4 system Asset Owners: stock, supplier, disposer, assembler', async () => {
            const stock = (await Owners.get(session.token, 'stock')).body;
            const assembler = (await Owners.get(session.token, 'assembler')).body;
            const disposer = (await Owners.get(session.token, 'disposer')).body;
            const supplier = (await Owners.get(session.token, 'supplier')).body;

            const systemAssetOwners = [stock, assembler, disposer, supplier];
            systemAssetOwners.forEach(el => {
                expect(el).toHaveProperty('id');
                expect(el).toHaveProperty('fullName');
                expect(el).toHaveProperty('email');
            });

            const allChecks = systemAssetOwners.map(async owner => {
                let response = await Owners.add(session.token, {
                    id: owner.id,
                    email: 'new-email@hacker.com',
                    fullName: 'Hello'
                });
                expect(response.statusCode).toEqual(422);

                response = await Owners.update(session.token, owner.id, {
                    email: 'new-email@hacker.com',
                    fullName: 'Hello'
                });
                expect(response.statusCode).toEqual(422);

                response = await Owners.remove(session.token, owner.id);
                expect(response.statusCode).toEqual(422);
            });

            await Promise.all(allChecks);

        });

        test('Owners CRUD', async () => {
            // create owner
            let response = await Owners.add(session.token, {
                email: 'rookie@example.com',
                fullName: 'Rookie'
            });
            expect(response.statusCode).toEqual(200);
            let rookie = response.body;

            // check really added
            expect(rookie).toMatchObject({
                id: expect.stringMatching(/\w{8,}/),
                email: 'rookie@example.com',
                fullName: 'Rookie'
            });

            const r2 = (await Owners.get(session.token, rookie.id)).body;
            expect(r2).toMatchObject(rookie);

            // update
            response = await Owners.update(session.token, rookie.id, {
                email: 'doe@example.com',
                fullName: 'John Doe'
            });
            expect(response.statusCode).toEqual(200);
            let updated = response.body;

            expect(updated).toMatchObject({
                email: 'doe@example.com',
                fullName: 'John Doe'
            });

            response = await Owners.remove(session.token, rookie.id);
            expect(response.statusCode).toEqual(200);

            // check deleted
            response = await Owners.get(session.token, rookie.id);
            expect(response.statusCode).toEqual(404);

            // delete not existing should be 200 too
            response = await Owners.remove(session.token, rookie.id);
            expect(response.statusCode).toEqual(200);

            // update non existing
            // update
            response = await Owners.update(session.token, rookie.id, {
                email: 'doe@example.com',
                fullName: 'John Doe'
            });
            expect(response.statusCode).toEqual(404);

        });

        test('Owners Search', async () => {
            let response = await Owners.search(session.token, {});
            expect(response.statusCode).toEqual(200);
            expect(response.body).toHaveLength(4); // all system owners

            await Owners.add(session.token, {
                email: 'rookie@example.com',
                fullName: 'Rookie'
            });

            response = await Owners.search(session.token, {first: 10});
            expect(response.statusCode).toEqual(200);
            expect(response.body).toHaveLength(5);

            response = await Owners.search(session.token, {first: 2});
            expect(response.statusCode).toEqual(200);
            expect(response.body).toHaveLength(2);

            response = await Owners.search(session.token, {first: 1, like: 'rook'});
            expect(response.statusCode).toEqual(200);
            expect(response.body).toHaveLength(1);
            expect(response.body[0]).toMatchObject({
                email: 'rookie@example.com',
                fullName: 'Rookie'
            })


        });

        test('Access control', async ()=> {
            const sample = {
                email: 'other@example.com',
                fullName: 'Sample User'
            };

            await Owners.add(session.token, sample);

            const testForRole = async (session) => {
                //
                // simple user can only search and get by id
                //
                let response = await Owners.search(session.token, {like: sample.fullName});
                expect(response.statusCode).toEqual(200);
                expect(response.body).toHaveLength(1);

                let savedSample = response.body[0];

                response = await Owners.get(session.token, savedSample.id);
                expect(response.statusCode).toEqual(200);

                ///
                // rest should require admin role
                //

                response = await Owners.add(session.token, sample);
                expect(response.statusCode).toEqual(403); // forbidden
                response = await Owners.add('invalid token', sample);
                expect(response.statusCode).toEqual(401); // unauthorized

                response = await Owners.update(session.token, savedSample.id, sample);
                expect(response.statusCode).toEqual(403); // forbidden
                response = await Owners.update('invalid token', savedSample.id, sample);
                expect(response.statusCode).toEqual(401); // unauthorized

                response = await Owners.remove(session.token, savedSample.id);
                expect(response.statusCode).toEqual(403); // forbidden
                response = await Owners.remove('invalid token', savedSample.id);
                expect(response.statusCode).toEqual(401); // unauthorized
            };

            await testForRole((await Auth.userLogin()).body);
            await testForRole((await Auth.reportViewerLogin()).body)
        })
    });


};
