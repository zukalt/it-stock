module.exports = ({Auth}) => {

    describe('Log In/Out', () => {
        let session;

        it('should refuse to log in with wrong credentials', async () => {

            let res = await Auth.login('admin@example.com');
            expect(res.statusCode).toEqual(401);
            expect(res.body.token).toBeFalsy();

            res = await Auth.login('admin@example.com', 'wrong pass');
            expect(res.statusCode).toEqual(401);
            expect(res.body.token).toBeFalsy();
        });

        it('should log in with correct credentials', async () => {
            const res = await Auth.login( 'admin@example.com', 'password');
            session = res.body;
            expect(res.statusCode).toEqual(200);
            expect(session).toMatchObject({
                token: expect.stringMatching(/\w{8,}/),
                user: expect.objectContaining({
                    email: 'admin@example.com',
                    fullName: expect.stringMatching(/\w+/),
                    role: expect.stringMatching(/(AUTHENTICATED|ADMIN|REPORT_VIEWER)/)
                })
            });
        });

        it('should log out properly', async () => {
            // check logged in
            let me = await Auth.whoami(session.token);

            expect(me.statusCode).toEqual(200);
            expect(me.body).toMatchObject(session.user);

            let logout = await Auth.logout(session.token);

            expect(logout.statusCode).toEqual(200);

            let res = await Auth.whoami(session.token);
            expect(res.statusCode).toEqual(401);
            expect(res.body).toMatchObject({"error": "UnAuthenticatedError"});
        });

        it('should be silent on double logout or invalid token', async ()=>{
            let logout = await Auth.logout('invalid token');
            expect(logout.statusCode).toEqual(200);
        });
    });
};
