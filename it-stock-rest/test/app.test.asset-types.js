module.exports = ({Auth, AssetTypes}) => {

    describe('Reference Types Management  AssetTypes', () => {

        let session;

        beforeAll(async () => {
            let res = await Auth.adminLogin();
            expect(res.statusCode).toEqual(200);
            session = res.body;
        });


        test('Asset Types Crud', async () => {
            // create owner
            let response = await AssetTypes.add(session.token, {
                category: 'KEYBOARD',
                name: 'Genius'
            });
            expect(response.statusCode).toEqual(200);
            let assetType = response.body;

            // check really added
            expect(assetType).toMatchObject({
                id: expect.stringMatching(/\w{8,}/),
                category: 'KEYBOARD',
                name: 'Genius'
            });

            const loaded = (await AssetTypes.get(session.token, assetType.id)).body;
            expect(loaded).toMatchObject(assetType);

            // update
            let updated = await AssetTypes.update(session.token, assetType.id, {
                category: 'KEYBOARD',
                name: 'HP'
            });
            expect(updated.statusCode).toEqual(200);

            expect(updated.body).toMatchObject({
                category: 'KEYBOARD',
                name: 'HP'
            });

            response = await AssetTypes.remove(session.token, assetType.id);
            expect(response.statusCode).toEqual(200);

            // check deleted
            response = await AssetTypes.get(session.token, assetType.id);
            expect(response.statusCode).toEqual(404);

            // delete not existing should be 200 too
            response = await AssetTypes.remove(session.token, assetType.id);
            expect(response.statusCode).toEqual(200);

            // update non existing
            // update
            response = await AssetTypes.update(session.token, assetType.id, {
                category: 'KEYBOARD',
                name: 'Lenovo'
            });
            expect(response.statusCode).toEqual(404);
        });

        test('Assets Search', async () => {

            const fixture = [
                {category: "KEYBOARD", name: "HP model x"},
                {category: "KEYBOARD", name: "Lenovo model y"},
                {category: "KEYBOARD", name: "Apple keyboard"},
                {category: "UPS", name: "Mercury A1000"}
            ];

            await Promise.all(fixture.map(a => AssetTypes.add(session.token, a))) ;


            let response = await AssetTypes.search(session.token, {});
            expect(response.statusCode).toEqual(200);
            expect(response.body).toHaveLength(4); // all fixtures


            response = await AssetTypes.search(session.token, {first: 10});
            expect(response.statusCode).toEqual(200);
            expect(response.body).toHaveLength(4);

            response = await AssetTypes.search(session.token, {first: 2});
            expect(response.statusCode).toEqual(200);
            expect(response.body).toHaveLength(2);

            response = await AssetTypes.search(session.token, {first: 1, like: 'a100'});
            expect(response.statusCode).toEqual(200);
            expect(response.body).toHaveLength(1);
            expect(response.body[0]).toMatchObject({category: "UPS", name: "Mercury A1000"});

            response = await AssetTypes.search(session.token, {first: 10, like: 'ups'});
            expect(response.statusCode).toEqual(200);
            expect(response.body).toHaveLength(1);
            expect(response.body[0]).toMatchObject({category: "UPS", name: "Mercury A1000"});

        });

        test('Access control', async ()=> {

            const testForRole = async (session) => {
                //
                // simple user can only search and get by id
                //
                let response = await AssetTypes.search(session.token, {});
                expect(response.statusCode).toEqual(200);
                expect(response.body).toHaveLength(4); // all fixtures

                let assetType = response.body[0];

                response = await AssetTypes.get(session.token, assetType.id);
                expect(response.statusCode).toEqual(200);

                ///
                // rest should require admin role
                //
                response = await AssetTypes.add(session.token, {category: 'A', name: 'B'});
                expect(response.statusCode).toEqual(403); // forbidden
                response = await AssetTypes.add('invalid token', {category: 'A', name: 'B'});
                expect(response.statusCode).toEqual(401); // unauthorized

                response = await AssetTypes.update(session.token, assetType.id, {category: 'A', name: 'B'});
                expect(response.statusCode).toEqual(403); // forbidden
                response = await AssetTypes.update('invalid token', assetType.id, {category: 'A', name: 'B'});
                expect(response.statusCode).toEqual(401); // unauthorized

                response = await AssetTypes.remove(session.token, assetType.id);
                expect(response.statusCode).toEqual(403); // forbidden
                response = await AssetTypes.remove('invalid token', assetType.id);
                expect(response.statusCode).toEqual(401); // unauthorized
            };

            await testForRole((await Auth.userLogin()).body);
            await testForRole((await Auth.reportViewerLogin()).body)
        });


    });


};
