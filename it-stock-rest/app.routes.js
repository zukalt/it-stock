const Router = require('express').Router;
const {
  appAuth,
  session,
  appAssetTypes,
  appAssetOwners,
  appTransfers
} = require('./app.config');
const {UnAuthenticatedError} = require('it-stock-core/model');

const getToken = req => {
  let token = req.header('Authorization');
  if (!token) {
    return token;
  }
  if (token.toLocaleLowerCase().startsWith("bearer ")) {
    token = token.substr("bearer ".length);
  }
  return token;
};

const addCurrentUser = (req, res, next) => {
  req.requestDto = req.requestDto || {};
  session.sessionGet(getToken(req))
    .then(user => {
      if (user) {
        req.requestDto.currentUser = user;
        next();
      }
      else {
        next(new UnAuthenticatedError());
      }
    })
    .catch(next);
};
const addPathParams = (req, res, next) => {
  // req.requestDto = req.requestDto || {};
  Object.assign(req.requestDto, req.params);
  next();
};

const addQueryParams = (req, res, next) => {
  // req.requestDto = req.requestDto || {};
  Object.assign(req.requestDto, req.query);
  next();
};

const addBodyAs = (key) => (req, res, next) => {
  req.requestDto = req.requestDto || {};
  if (key) {
    req.requestDto[key] = req.body;
  }
  else {
    Object.assign(req.requestDto, req.body);
  }
  next();
};

const asRouteHandler = (func) => {
  return (req, res, next) => func(req.requestDto).then(data => res.send(data)).catch(next);
};

const apiRouter = Router();

/* Auth */
var authRouter = Router();

apiRouter.use('/auth', authRouter);
authRouter.post('/login', addBodyAs(), asRouteHandler(appAuth.appLogin));
authRouter.post('/logout', (req, res, next) => {
  appAuth.appLogout({ token: getToken(req) })
    .then(() => res.sendStatus(200))
    .catch(next);
});
authRouter.get('/whoami', addCurrentUser, (req, res) => {
  res.send(req.requestDto.currentUser);
});

/* Owners */

const ownersRouter = Router();
apiRouter.use('/owners', ownersRouter);
ownersRouter.use(addCurrentUser);
ownersRouter.use(addBodyAs('owner'));

ownersRouter.get('/', addQueryParams, asRouteHandler(appAssetOwners.searchOwners));
ownersRouter.get('/:ownerId', addPathParams, asRouteHandler(appAssetOwners.getOwnerById));
ownersRouter.post('/', asRouteHandler(appAssetOwners.addOwner));
ownersRouter.patch('/:ownerId', addPathParams, asRouteHandler(appAssetOwners.updateOwner));
ownersRouter.delete('/:ownerId', addPathParams, asRouteHandler(appAssetOwners.deleteOwner));

/* Asset Types */

const assetsRouter = Router();
apiRouter.use('/assets', assetsRouter);
assetsRouter.use(addCurrentUser);
assetsRouter.use(addBodyAs('assetType'));

assetsRouter.get('/', addQueryParams, asRouteHandler(appAssetTypes.searchAssetTypes));
assetsRouter.get('/:assetTypeId', addPathParams, asRouteHandler(appAssetTypes.getAssetTypeById));
assetsRouter.post('/', asRouteHandler(appAssetTypes.addAssetType));
assetsRouter.patch('/:assetTypeId', addPathParams, asRouteHandler(appAssetTypes.updateAssetType));
assetsRouter.delete('/:assetTypeId', addPathParams, asRouteHandler(appAssetTypes.deleteAssetType));

const transfersRouter = Router();
apiRouter.use('/transfers', transfersRouter);
transfersRouter.use(addCurrentUser);
transfersRouter.use(addBodyAs('transfer'));

transfersRouter.get('/', addQueryParams, asRouteHandler(appTransfers.searchLog));
transfersRouter.post('/', asRouteHandler(appTransfers.saveTransfer));
transfersRouter.delete('/:transferId', addPathParams, asRouteHandler(appTransfers.undoTransfer));

// this is for both API format testing and 100% coverage of router error handler
apiRouter.use('/fail', asRouteHandler(async () => {throw new Error('error-format-check')}));

module.exports = apiRouter;
