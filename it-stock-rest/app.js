const express = require('express')
    , path = require('path')
    , logger = require('morgan')
    , helmet = require('helmet')
    , app = express()
    , apiRoutes = require('./app.routes')
    , {initApp} = require('./app.config');

const {
    UnAuthenticatedError,
    UnAuthorizedError,
    InvalidInputError,
    EntityNotFoundError
} = require('it-stock-core/model');


app.use(logger('dev'));
app.use(express.json());
app.use(helmet());
app.use('/api', apiRoutes);


app.use(/*jshint node:true, unused:false */function(err, req, res, next) {

    if (err instanceof UnAuthenticatedError) {
        res.status(401).send({ error: err.message }) // unauthorized: not logged in
    }
    else  if (err instanceof UnAuthorizedError) {
        res.status(403).send({ error: err.message }); // forbidden
    } 
    else  if (err instanceof EntityNotFoundError) {
        res.status(404).send({ error: err.message }); // not found
    } 
    else  if (err instanceof InvalidInputError) {
        res.status(422).send({ error: err.message }); // unprocessed
    } 
    else {
        // console.log(err);
        res.status(500).send({ error: err.message });
    } 
});


// serve local built static files if requested
const spaDir = path.join(__dirname, '..', 'it-stock-web', 'build');
const index_html = path.join(spaDir , 'index.html');
app.use(express.static(spaDir));
app.get('/*', (req, res) => {
    res.sendFile(index_html);
});


module.exports = {
    app,
    initApp,
};
