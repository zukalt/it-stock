const {generateIdFrom} = require('./utils');

class StockAssetOwner {
    constructor(fullName, email, id) {
        this.email = email;
        this.fullName = fullName;
        this.id = id;
    }

    static from({fullName, email, id}) {
        if (!email && !fullName) {
            throw new InvalidInputError('Either email or fullName is must be specified');
        }
        fullName = fullName ||  email.split('@')[0];

        return new StockAssetOwner(fullName, email, id || generateIdFrom(fullName));
    }

    to() {
        return {id: this.id, fullName: this.fullName, email: this.email}
    }
}


StockAssetOwner.STOCK = new StockAssetOwner('', '_stock@local', 'stock');
StockAssetOwner.ASSEMBLER = new StockAssetOwner('', '_assembler@local', 'assembler');
StockAssetOwner.SUPPLIER = new StockAssetOwner('', '_procurement@local', 'supplier');
StockAssetOwner.DISPOSER = new StockAssetOwner('', '_disposer@local', 'disposer');


module.exports = StockAssetOwner;
