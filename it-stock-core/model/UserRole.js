class UserRole {
    /**
     *
     * @param {string} name
     * @param {string} level
     */
    constructor(name, level) {
        this.name = name;
        this.level = level;
    }

    /**
     *
     * @param {string} role string representation of role
     * @returns {UserRole}
     */
    static from(role) {
        role = role || 'authenticated';
        switch (role.toLowerCase().substr(0, 2)) {
            case 'au':
                role = UserRole.AUTHENTICATED;
                break;
            case 'ad':
                role = UserRole.ADMIN;
                break;
            case 're':
                role = UserRole.REPORT_VIEWER;
                break;
            default:
                throw new Error("Can't guess role")
        }
        return role;
    }

    to() {
        return this.name;
    }


}

UserRole.AUTHENTICATED = new UserRole('AUTHENTICATED', 0);
UserRole.REPORT_VIEWER = new UserRole('REPORT_VIEWER', 10);
UserRole.ADMIN = new UserRole('ADMIN', 20);

module.exports = UserRole;
