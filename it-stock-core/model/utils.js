const crypto = require('crypto');

const generateIdFrom  = (...args) => {
    const sha1 = crypto.createHash('sha1');
    args.forEach(e => sha1.update(e));
    return sha1.digest().toString('hex')
};

const generateId = (size = 16) =>{
    return crypto.randomBytes(size).toString('hex');
};

module.exports = {
    generateIdFrom,
    generateId
};
