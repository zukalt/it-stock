class StockAssetCondition {
    constructor(name) {
        this.name = name;
    }

    static from(name) {
        return new StockAssetCondition(name);
    }

    to() {
        return this.name;
    }
}

StockAssetCondition.NEW = new StockAssetCondition('NEW');
StockAssetCondition.UNKNOWN = new StockAssetCondition('UNKNOWN');
StockAssetCondition.USED = new StockAssetCondition('USED');


module.exports = StockAssetCondition;
