const {generateIdFrom} = require('./utils');

class StockAssetType {
    constructor(category, name, id) {
        this.category = category;
        this.name = name;
        this.id = id;
    }

    static from({id, category, name}) {
        return new StockAssetType(category, name, id || generateIdFrom(category, name))
    }

    to() {
        return {id: this.id, name: this.name, category: this.category}
    }
}

module.exports = StockAssetType;
