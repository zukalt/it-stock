const UserRole = require('./UserRole')
    , {InvalidInputError} = require('./exceptions');

class User {
    /**
     *
     * @param {string} email
     * @param {string} fullName
     * @param {UserRole} role
     */
    constructor(email, fullName, role = UserRole.AUTHENTICATED) {
        this.email = email;
        this.fullName = fullName;
        this.role = role;
    }

    /**
     *
     * @param {string} email
     * @param {string} fullName
     * @param {string }role
     * @returns {User}
     */
    static from({email, fullName, role}) {
        if (!email) {
            throw new InvalidInputError('Email is mandatory')
        }
        if (!fullName) {
            fullName = email.split('@')[0];
        }
        return new User(email, fullName, UserRole.from(role));
    }

    to() {
        return {
            email: this.email,
            fullName: this.fullName,
            role: this.role.to()
        }
    }
}

module.exports = User;
