const StockAssetOwner = require("./StockAssetOwner")
    , StockAssetCondition = require("./StockAssetCondition")
    , {InvalidInputError} = require('./exceptions');

class StockAssetTransfer {

    constructor(fromOwner, toOwner, assetType, condition, quantity, date, serial, notes, id) {
        this.fromOwner = fromOwner;
        this.toOwner = toOwner;
        this.assetType = assetType;
        this.condition = condition;
        this.quantity = quantity;
        this.date = date;
        this.serial = serial;
        this.notes = notes;
        this.id = id;
    }

    to() {
        return {
            id: this.id,
            fromOwner: this.fromOwner,
            toOwner: this.toOwner,
            assetType: this.assetType,
            condition: this.condition.to(),
            quantity: this.quantity,
            date: this.date,
            serial: this.serial,
            notes: this.notes
        }
    }

    static from({fromOwner, toOwner, assetType, condition, quantity, date, serial, notes, id}) {

        StockAssetTransfer.validateRequiredFields({fromOwner, toOwner, assetType, date});

        fromOwner = fromOwner || StockAssetOwner.SUPPLIER.id;
        toOwner = toOwner || StockAssetOwner.DISPOSER.id;

        condition = StockAssetCondition.from(condition || 'UNKNOWN');

        if (!Number.isInteger(quantity)) {
            quantity = 1;
        }

        return new StockAssetTransfer(fromOwner, toOwner, assetType, condition, quantity, date, serial, notes, id);
    }

    static validateRequiredFields({fromOwner, toOwner, assetType, date}) {
        if (!Number.isSafeInteger(date)) {
            throw new InvalidInputError('Transfer date is mandatory');
        }
        if (!fromOwner && !toOwner) {
            throw new InvalidInputError('Either fromOwner or toOwner field must be supplied');
        }

        if (!assetType) {
            throw new InvalidInputError('AssetType is mandatory');
        }
    }
}

module.exports = StockAssetTransfer;
