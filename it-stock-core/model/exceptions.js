
class UnAuthenticatedError extends Error {
    constructor() {
        super('UnAuthenticatedError')
    }
}

class UnAuthorizedError extends Error {
    constructor() {
        super('UnAuthorizedError')
    }
}

class InvalidInputError extends Error {
    constructor(message) {
        super('InvalidInputError: ' + message)
    }
}

class EntityNotFoundError extends Error {
    constructor(message) {
        super('EntityNotFoundError: ' + message)
    }
}

module.exports = {
    UnAuthenticatedError,
    UnAuthorizedError,
    InvalidInputError,
    EntityNotFoundError
} ;
