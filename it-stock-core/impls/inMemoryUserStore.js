const {User} = require('../model');
const {generateIdFrom} = require('../model/utils');

module.exports = (users) => {

    const passwordStore = {};
    const profileStore = {};

    users.forEach(user => {
        profileStore[user.email] = User.from(user);
        passwordStore[user.email] = generateIdFrom(user.password || 'password');
    });

    const authenticate = async (email, password) => password && (passwordStore[email] === generateIdFrom(password));

    const loadProfile = async (email) => profileStore[email].to();

    return {
        authenticate, loadProfile
    }

};
