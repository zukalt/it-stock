const InMemoryStore = require('./inMemoryObjectStore') ;
module.exports = (initialValues) => {
    return new InMemoryStore(['fullName'], initialValues || {});
};
