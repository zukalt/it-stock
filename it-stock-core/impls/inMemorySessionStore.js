const crypto = require('crypto');

const store = {};
const config = {
    ttl: 3600000,
    maxActiveSessionsAllowed: 5
};

const sessionInit = sessionConfig => {
    Object.assign(config, sessionConfig)
};

const sessionStart = async (user) => {
    const id = crypto.randomBytes(32).toString('base64');
    store[id] = {
        token: id,
        // created: Date.now(),
        accessed: Date.now(),
        user: user
    };

    const allSessions = [];
    for (let [token, session] of Object.entries(store)) {
        if (session.user.id === user.id) {
            if (session.accessed + config.ttl > Date.now()) {
                allSessions.push(session);
            }
            else {
                await sessionInvalidate(token);
            }
        }
    }

    if (allSessions.length > config.maxActiveSessionsAllowed) {
        allSessions.sort((s1, s2) => s1.accessed - s2.accessed);
        for (let i = 0; i < allSessions.length - config.maxActiveSessionsAllowed; i++) {
            await sessionInvalidate(allSessions[i].token)
        }
    }
    return { token: id, user: user };
};

const sessionGet = async id => {
    const session = store[id];
    if (!session) {
        return null;
    }
    const now = Date.now();
    if (session.accessed + config.ttl < now) {
        delete store[id];
        return null;
    }
    session.accessed = now;
    return { ...session.user };
};

const sessionInvalidate = async id => {
    delete store[id];
};


module.exports = function init(config) {
    sessionInit(config);
    return {
        sessionStart,
        sessionGet,
        sessionInvalidate
    }
};
