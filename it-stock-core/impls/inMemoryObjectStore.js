const { generateId } = require('../model/utils');

class InMemoryStore {

    constructor(searchFields, initialData) {
        this.searchFields = searchFields;
        this.store = initialData || {};

        this.save = async ({ entity }) => {
            const saved = { ...entity };
            saved.id = saved.id || generateId();
            this.store[saved.id] = saved;
            return saved;
        };

        this.remove = async ({id}) => {
            delete this.store[id];
        };

        this.searchInFields = async ({ fields, like, limit }) => {
            like = like ? like.toLowerCase() : null;
            return Object.keys(this.store)
                .map(id => this.store[id])
                .filter(
                    entity => {
                        let found = true;
                        if (like) {
                            found = fields.find(prop => `${entity[prop]}`.toLowerCase().indexOf(like) > -1)
                        }
                        return found && (--limit >= 0);
                    }
                );
        };

        this.search = async ({ like, limit }) => {
            return this.searchInFields({ fields: this.searchFields, like, limit })
        };

        this.getById = async ({ id }) => {
            if (this.store[id]) {
                return { ...this.store[id] }
            }
            return null;
        }
    }
}


module.exports = InMemoryStore;
