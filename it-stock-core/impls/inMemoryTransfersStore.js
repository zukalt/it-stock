const InMemoryStore = require('./inMemoryObjectStore') ;

class InMemoryTransfersStore extends  InMemoryStore {
    constructor(initialValues){
        super([], initialValues);

        this.containsTransferForOwner = ({ownerId}) => this.contains(t=>t.fromOwner === ownerId || t.toOwner===ownerId);

        this.containsTransferForAssetType = ({assetTypeId}) => this.contains(t=>t.assetType === assetTypeId);

        this.search = async ({ownerId, assetTypeId}) => {
            return this.items()
                .filter(t=> !ownerId || t.fromOwner === ownerId || t.toOwner === ownerId)
                .filter(t=> !assetTypeId || t.assetType === assetTypeId)
        };
    }

    items() {
        return Object.keys(this.store).map(k=>this.store[k]);
    }

    contains(predicate) {
        return this.items().findIndex(predicate) > -1;
    }
}


module.exports = (initialValues) => {
    return new InMemoryTransfersStore( initialValues || {});
};
