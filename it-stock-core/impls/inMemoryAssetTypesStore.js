const InMemoryStore = require('./inMemoryObjectStore') ;
module.exports = (initialValues) => {
    return new InMemoryStore(['name', 'category'], initialValues || {});
};
