const {
    User,
    UserRole,
    StockAssetType,
    StockAssetOwner,
    StockAssetTransfer,
    StockAssetCondition,
    InvalidInputError
} = require('../model');

describe('class User {', () => {

    test('constructor', () => {
        let user = new User('example@example.com');
        expect(user.role).toMatchObject(UserRole.AUTHENTICATED);
    });

    test('User.from ()', () => {
        // email is mandatory
        const newUser = () => User.from({fullName: 'Jane Doe', role: 'admin'});
        expect(newUser).toThrow(InvalidInputError);

        // should derive fullName from email
        let user = User.from({email: 'example@mail.com'});
        expect(user.fullName).toEqual('example');
    });

    test('.to', () => {
        let user = {fullName: 'Jane Doe', email: 'user@app.com', role: 'admin'};
        let userClass = User.from(user);

        user.role = user.role.toUpperCase();
        expect(userClass.to()).toMatchObject(user);
    });
});

describe('class UserRole {', () => {

    test('constructor', () => {
        let role = new UserRole('CustomRole', -1);
        expect(role.name).toEqual('CustomRole');
        expect(role.level).toBe(-1);
    });

    test('constant instances', () => {
        expect(UserRole.AUTHENTICATED).toBeDefined();
        expect(UserRole.REPORT_VIEWER).toBeDefined();
        expect(UserRole.ADMIN).toBeDefined();
    });

    test('UserRole.from ()', () => {
        expect(UserRole.from('re')).toEqual(UserRole.REPORT_VIEWER);
        expect(UserRole.from('ad')).toEqual(UserRole.ADMIN);
        expect(UserRole.from('au')).toEqual(UserRole.AUTHENTICATED);
        expect(() => UserRole.from('x')).toThrow(Error);
    });

    test('.to', () => {
        expect(UserRole.AUTHENTICATED.to()).toEqual('AUTHENTICATED');
        expect(UserRole.REPORT_VIEWER.to()).toEqual('REPORT_VIEWER');
        expect(UserRole.ADMIN.to()).toEqual('ADMIN');
    });
});

describe('class StockAssetType {', () => {

    test('constructor', () => {
        let assetType = new StockAssetType('cat', 'name', 'id');
        expect(assetType.to()).toEqual({category: 'cat', name: 'name', id: 'id'});
    });

    test('.from', () => {
        let assetType = StockAssetType.from({category: 'cat', name: 'name', id: 'id'});
        expect(assetType).toMatchObject({category: 'cat', name: 'name', id: 'id'});

        assetType = StockAssetType.from({category: 'cat', name: 'name'});
        let assetType2 = StockAssetType.from({category: 'cat', name: 'name'});
        // should generate global uuid (hash) automatically
        expect(assetType).toMatchObject({category: 'cat', name: 'name', id: expect.stringMatching(/\w{8,}/)});
        // generated id should be same for given category and name
        expect(assetType).toEqual(assetType2);
    });

    test('.to', () => {
        let assetType = {category: 'cat', name: 'name', id: 'id'};
        expect(StockAssetType.from(assetType).to()).toMatchObject(assetType);
    });
});

describe('class StockAssetOwner {', () => {

    test('constructor', () => {
        // variables order
        let owner = new StockAssetOwner('fullName', 'email', 'id');
        expect(owner.id).toBe('id');
        expect(owner.fullName).toBe('fullName');
        expect(owner.email).toBe('email');
    });


    test('.from', () => {
        let owner = StockAssetOwner.from({fullName: "fullName"});
        let owner2 = StockAssetOwner.from({fullName: "fullName", email: 'email'});
        expect(owner.fullName).toBe('fullName');
        expect(owner.email).toBeUndefined();
        expect(owner.id).toBeDefined();
        expect(owner2.id).toBeDefined();
        // id should derived from fullName
        expect(owner.id).toBe(owner2.id);

        // fullName derivation from email
        owner = StockAssetOwner.from({email: "someone@example.com"});
        expect(owner.fullName).toBe('someone');

        // fullName is mandatory
        expect(() => StockAssetOwner.from({id: 'id'})).toThrow(Error);
    });

    test('.to', () => {
        let owner = StockAssetOwner.from({email: 'email', fullName: 'fullName', id: 'id'});
        expect(owner.to()).toEqual({email: 'email', fullName: 'fullName', id: 'id'});
    });
});


describe('class StockAssetCondition {', () => {
    test('constructor', ()=>{
        let condition = new StockAssetCondition('X');
        expect(condition.name).toBe('X');
    });
    test('StockAssetCondition.from(name)', () => {
        let condition = StockAssetCondition.from('A');
        expect(condition.name).toBe('A');
    });

    test('.to', ()=>{
        let condition = new StockAssetCondition('X');
        expect(condition.to()).toEqual('X');
    });
}) ;


describe('class StockAssetTransfer {', () => {

    test('constructor', () => {
        let transfer = new StockAssetTransfer('ownerId_1', 'ownerId_2', 'assetTypeId_1', StockAssetCondition.NEW, 0, 1, 'serial', 'notes');
        expect(transfer.fromOwner).toBe('ownerId_1');
        expect(transfer.toOwner).toBe('ownerId_2');
        expect(transfer.assetType).toBe('assetTypeId_1');
        expect(transfer.condition).toBe(StockAssetCondition.NEW);
        expect(transfer.quantity).toBe(0);
        expect(transfer.date).toBe(1);
        expect(transfer.serial).toBe('serial');
        expect(transfer.notes).toBe('notes');
    });


    test('StockAssetTransfer.from && .to', () => {

        // if destination not set, then items is to be sent to disposer (remove from stock)
        let transfer = StockAssetTransfer.from({
            fromOwner: 'A', assetType: 'assetTypeId', date: 0
        });
        expect(transfer.toOwner).toEqual(StockAssetOwner.DISPOSER.id);
        expect(transfer.quantity).toEqual(1);
        expect(transfer.condition).toEqual(StockAssetCondition.UNKNOWN);

        // if source is not set, then its new (newly bought)
        transfer = StockAssetTransfer.from({
            toOwner: 'A', assetType: 'assetTypeId', date: 0, condition: 'NEW', extraField: 'ignored'
        });
        expect(transfer.fromOwner).toEqual(StockAssetOwner.SUPPLIER.id);
        expect(transfer.condition).toEqual(StockAssetCondition.NEW);
        expect(transfer.extraField).toBeUndefined(); // extra fields to be ignored

        let sample = {
            fromOwner: 'A',
            toOwner: 'B',
            assetType: 'assetTypeId',
            condition: 'NEW',
            date: 0,
            quantity: 10, notes: 'some notes', serial: 'unknown serial'
        };

        expect(StockAssetTransfer.from(sample).to()).toEqual(sample) ;

    });

    test('StockAssetTransfer.validateRequiredFields', () => {
        // all OK variations, those should not fail
        StockAssetTransfer.validateRequiredFields({
            fromOwner: 'A', assetType: 'assetTypeId', date: 0
        });
        StockAssetTransfer.validateRequiredFields({
            fromOwner: 'A', assetType: 'assetTypeId', date: 0, otherField: 'ignored'
        });
        StockAssetTransfer.validateRequiredFields({
            toOwner: 'A', assetType: 'assetTypeId', date: 0
        });
        StockAssetTransfer.validateRequiredFields({
            fromOwner: 'B', toOwner: 'A', assetType: 'assetTypeId', date: 0
        });

        // error cases
        expect(()=>StockAssetTransfer.validateRequiredFields({
            fromOwner: 'B', toOwner: 'A', assetType: 'assetTypeId' // no date
        })).toThrow(InvalidInputError);

        expect(()=>StockAssetTransfer.validateRequiredFields({
            fromOwner: 'B', toOwner: 'A',  date: 0 // no assetType
        })).toThrow(InvalidInputError);
        expect(()=>StockAssetTransfer.validateRequiredFields({
             assetType: 'assetTypeId', date: 0 // no date
        })).toThrow(InvalidInputError);
    });

});


