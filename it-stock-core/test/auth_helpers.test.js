const {
    requireAdmin, requireReportViewer, requireAuthenticated
} = require('../app/auth_helpers') ;
const {UnAuthenticatedError, UnAuthorizedError} = require('../model') ;

describe('Auth helpers', ()=>{


    test('requireAdmin (currentUser)', ()=>{
        expect(()=>requireAdmin(null)).toThrow(UnAuthenticatedError);
        expect(()=>requireAdmin({})).toThrow(UnAuthenticatedError);
        expect(()=>requireAdmin({role: undefined})).toThrow(UnAuthenticatedError);
        expect(()=>requireAdmin({role: null})).toThrow(UnAuthenticatedError);
        expect(()=>requireAdmin({role: 'X'})).toThrow(Error);
        expect(()=>requireAdmin({role: 'AUTHENTICATED'})).toThrow(UnAuthorizedError);
        expect(()=>requireAdmin({role: 'AU'})).toThrow(UnAuthorizedError);
        expect(()=>requireAdmin({role: 'RE'})).toThrow(UnAuthorizedError);
        expect(()=>requireAdmin({role: 'REPORT_VIEWER'})).toThrow(UnAuthorizedError);
        requireAdmin({role: 'AD'});
        requireAdmin({role: 'ADMIN'});
    });

    test('requireReportViewer (currentUser)', ()=>{
        expect(()=>requireReportViewer(null)).toThrow(UnAuthenticatedError);
        expect(()=>requireReportViewer({})).toThrow(UnAuthenticatedError);
        expect(()=>requireReportViewer({role: undefined})).toThrow(UnAuthenticatedError);
        expect(()=>requireReportViewer({role: null})).toThrow(UnAuthenticatedError);
        expect(()=>requireReportViewer({role: 'X'})).toThrow(Error);
        expect(()=>requireReportViewer({role: 'AUTHENTICATED'})).toThrow(UnAuthorizedError);
        expect(()=>requireReportViewer({role: 'AU'})).toThrow(UnAuthorizedError);
        requireReportViewer({role: 'RE'});
        requireReportViewer({role: 'REPORT_VIEWER'});
        requireReportViewer({role: 'AD'});
        requireReportViewer({role: 'ADMIN'});
    });

    test('requireAuthenticated (currentUser)', ()=>{
        expect(()=>requireAuthenticated(null)).toThrow(UnAuthenticatedError);
        expect(()=>requireAuthenticated({})).toThrow(UnAuthenticatedError);
        expect(()=>requireAuthenticated({role: undefined})).toThrow(UnAuthenticatedError);
        expect(()=>requireAuthenticated({role: null})).toThrow(UnAuthenticatedError);
        expect(()=>requireAuthenticated({role: 'X'})).toThrow(Error);
        requireAuthenticated({role: 'AU'});
        requireAuthenticated({role: 'AUTHENTICATED'});
        requireAuthenticated({role: 'RE'});
        requireAuthenticated({role: 'REPORT_VIEWER'});
        requireAuthenticated({role: 'AD'});
        requireAuthenticated({role: 'ADMIN'});
    });

}) ;
