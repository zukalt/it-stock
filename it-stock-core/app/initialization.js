const { StockAssetOwner } = require('../model');

module.exports = ({ ownerStore }) => {

    const mandatoryOwnersToBe = [
        StockAssetOwner.STOCK,
        StockAssetOwner.ASSEMBLER,
        StockAssetOwner.SUPPLIER,
        StockAssetOwner.DISPOSER
    ];

    const checkAndUpdateOwner = async (owner) => {
        if (!await ownerStore.getById({ id: owner.id })) {
            await ownerStore.save({ entity: owner });
        }
    };

    return Promise.all(mandatoryOwnersToBe.map((owner) => checkAndUpdateOwner(owner)))

};
