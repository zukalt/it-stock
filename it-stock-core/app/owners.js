const  {requireAdmin, requireAuthenticated} = require('./auth_helpers');
const {InvalidInputError, EntityNotFoundError, StockAssetOwner} = require('../model');

const IMMUTABLE_OWNERS = [
    StockAssetOwner.STOCK, StockAssetOwner.ASSEMBLER, StockAssetOwner.SUPPLIER, StockAssetOwner.DISPOSER
].map (o=>o.id) ;

const  checkNotImmutable = (ownerId) => {
    if (IMMUTABLE_OWNERS.find(id => id===ownerId)) {
        throw new InvalidInputError(`Asset owner ${ownerId} is immutable`) ;
    }
};

const addOwner = async ({ownerStore}, {currentUser, owner}) => {
    requireAdmin(currentUser) ;
    checkNotImmutable(owner.id);
    return await ownerStore.save({entity: StockAssetOwner.from(owner).to()});
};

const updateOwner = async({ownerStore}, {currentUser, ownerId, owner}) => {
    requireAdmin(currentUser);
    checkNotImmutable(ownerId);
    let stored = await ownerStore.getById({id: ownerId}) ;
    if (!stored) {
        throw  new EntityNotFoundError(`id: ${ownerId}`) ;
    }
    let entity = {
        ...owner,
        id: stored.id
    };
    return await ownerStore.save({entity}) ;
};

const deleteOwner = async ({ownerStore, assetTransferStore}, {currentUser, ownerId}) => {
    requireAdmin(currentUser);
    checkNotImmutable(ownerId);
    const used = await assetTransferStore.containsTransferForOwner({ownerId}) ;
    if (used) {
        throw  new InvalidInputError('Owner is referred in transfer') ;
    }
    return await ownerStore.remove({id: ownerId});
};

const getOwnerById = async ({ownerStore}, {currentUser, ownerId}) => {
    requireAuthenticated(currentUser) ;
    let entity = await ownerStore.getById({id: ownerId}) ;
    if (!entity) {
        throw new EntityNotFoundError(`id: ${ownerId}`)
    }
    return entity;
};

const searchOwners = async ({ownerStore}, {currentUser, like, first}) => {
    requireAuthenticated(currentUser) ;
    const limit  = first || 10;
    return await ownerStore.search({like: like, limit: limit});
};

module.exports = function assetsFactory ({ownerStore, assetTransferStore}) {

    return {
        addOwner: addOwner.bind(null, {ownerStore}),
        updateOwner: updateOwner.bind(null, {ownerStore}),
        deleteOwner: deleteOwner.bind(null, {ownerStore, assetTransferStore}),
        getOwnerById:  getOwnerById.bind(null, {ownerStore}),
        searchOwners:  searchOwners.bind(null, {ownerStore}),
    } ;
};
