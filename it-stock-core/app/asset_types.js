const  {requireAdmin, requireAuthenticated} = require('./auth_helpers');
const {StockAssetType, InvalidInputError, EntityNotFoundError} = require('../model');

const addAssetType = async ({assetTypeStore}, {currentUser, assetType}) => {
    requireAdmin(currentUser) ;
    assetType = StockAssetType.from(assetType).to();
    return await assetTypeStore.save({entity: assetType});
};

const updateAssetType = async({assetTypeStore}, {currentUser, assetTypeId, assetType}) => {
    requireAdmin(currentUser);

    let entity = await assetTypeStore.getById({id: assetTypeId}) ;
    if (!entity) {
        throw new EntityNotFoundError(`id: ${assetTypeId}`);
    }

    entity = StockAssetType.from({
        ...assetType,
        id: entity.id
    }).to();

    return await assetTypeStore.save({entity});
};

const deleteAssetType = async ({assetTypeStore, assetTransferStore}, {currentUser, assetTypeId}) => {
    requireAdmin(currentUser);
    const used = await assetTransferStore.containsTransferForAssetType({assetTypeId}) ;
    if (used) {
        throw  new InvalidInputError('Asset type is used in transfer') ;
    }
    return await assetTypeStore.remove({id: assetTypeId});
};

const getAssetTypeById = async ({assetTypeStore}, {currentUser, assetTypeId}) => {
    requireAuthenticated(currentUser) ;
    const entity = await assetTypeStore.getById({id: assetTypeId}) ;
    if (!entity) {
        throw new EntityNotFoundError(`id: ${assetTypeId}`);
    }
    return entity;
};

const searchAssetTypes = async ({assetTypeStore}, {currentUser, like, first}) => {
    requireAuthenticated(currentUser) ;
    const limit  = first || 10;
    return await assetTypeStore.search({like: like, limit: limit});
};

module.exports = function assetsFactory ({assetTypeStore, assetTransferStore}) {

    return {
        addAssetType: addAssetType.bind(null, {assetTypeStore}),
        updateAssetType: updateAssetType.bind(null, {assetTypeStore}),
        deleteAssetType: deleteAssetType.bind(null, {assetTypeStore, assetTransferStore}),
        getAssetTypeById:  getAssetTypeById.bind(null, {assetTypeStore}),
        searchAssetTypes:  searchAssetTypes.bind(null, {assetTypeStore}),
    } ;
};
