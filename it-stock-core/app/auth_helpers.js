const { UnAuthenticatedError, UnAuthorizedError, UserRole  } =  require('../model');
function requireRole(user, role) {
    if (!user || !user.role) {
        throw new UnAuthenticatedError();
    }
    if (UserRole.from(user.role).level < role.level) {
        throw new UnAuthorizedError();
    }
}

function requireAuthenticated(user) {
    requireRole (user, UserRole.AUTHENTICATED);
}

function requireReportViewer(user) {
    requireRole (user, UserRole.REPORT_VIEWER);
}

function requireAdmin(user) {
    requireRole (user, UserRole.ADMIN);
}
module.exports = {
    requireAdmin,
    requireReportViewer,
    requireAuthenticated
};
