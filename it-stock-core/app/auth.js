
const {UnAuthenticatedError} = require('../model');
const appLogin = async ({ authenticate, sessionStart,  loadProfile } , { email, password }) => {
    
    if (await authenticate(email, password)) {
        const profile = await loadProfile(email);
        return await sessionStart(profile);
    }
    
    throw new UnAuthenticatedError();
};

const appLogout = async ({ sessionInvalidate }, { token }) => {
    await sessionInvalidate(token);
};

module.exports = function initAuth ({authenticate, loadProfile, session}) {
  
    return {
        appLogin: appLogin.bind(null, {authenticate, loadProfile, sessionStart: session.sessionStart}),
        appLogout: appLogout.bind(null, {sessionInvalidate: session.sessionInvalidate})
    }
};
