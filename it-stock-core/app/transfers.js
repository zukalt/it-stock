const  {requireAdmin, requireReportViewer} = require('./auth_helpers');
const {StockAssetTransfer, StockAssetOwner, InvalidInputError} = require('../model');

const {SUPPLIER, STOCK, DISPOSER} = StockAssetOwner;

const saveTransfer = async ({assetTransferStore, assetTypeStore, ownerStore}, {currentUser, transfer}) => {
    requireAdmin(currentUser) ;
    let transferObj = StockAssetTransfer.from(transfer);
    if (await ownerStore.getById({id: transferObj.fromOwner}) === null) {
        throw new InvalidInputError('fromOwner not found');
    }
    else if (await ownerStore.getById({id: transferObj.toOwner}) === null) {
        throw new InvalidInputError('toOwner not found');
    }
    else if (await assetTypeStore.getById({id: transferObj.assetType}) === null) {
        throw new InvalidInputError('assetType not found');
    }

    if (transferObj.toOwner === SUPPLIER.id
        || transferObj.fromOwner === SUPPLIER.id && transferObj.toOwner !== STOCK.id
    ) {
        throw new InvalidInputError('Supplier can only transfer to Stock');
    }
    if (transferObj.fromOwner === DISPOSER.id) {
        throw new InvalidInputError('Disposer can only accept transfers');
    }


    return await assetTransferStore.save({entity: transferObj.to()}) ;
};

const undoTransfer = async ({assetTransferStore}, {currentUser, transferId}) => {
    requireAdmin(currentUser) ;
    return await assetTransferStore.remove({id: transferId}) ;
};

const searchLog = async ({assetTransferStore}, {currentUser, ownerId, assetTypeId}) => {
    requireReportViewer(currentUser) ;
    return await assetTransferStore.search({ownerId, assetTypeId}) ;
};


module.exports = ({assetTransferStore, assetTypeStore, ownerStore}) => {

    return {
        saveTransfer: saveTransfer.bind(null, {assetTransferStore, assetTypeStore, ownerStore}),
        undoTransfer: undoTransfer.bind(null, {assetTransferStore}),
        searchLog: searchLog.bind(null, {assetTransferStore}),
    };
};

