import React, {} from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';



export default function AppBarComponent({children}) {

    return (
        <AppBar position="static">
            <Toolbar>
                {children}
            </Toolbar>
        </AppBar>
    );
}

AppBarComponent.propTypes = {
    children: PropTypes.arrayOf(PropTypes.element).isRequired
};
