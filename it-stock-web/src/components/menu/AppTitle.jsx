import React from 'react';
import Typography from "@material-ui/core/Typography";

export default function AppTitle() {
    return (
        <Typography variant="h6" color="inherit">
            Stock
        </Typography>
    )
}
