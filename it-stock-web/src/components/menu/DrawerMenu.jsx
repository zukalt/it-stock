import React from 'react' ;

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

function ListItemLink({onSelect, path, ...props}) {
    return <ListItem button component="div"
                     onClick={() => onSelect(path)} {...props} />;
}

function Category({category, onSelect}) {
    return (
        <div className="width100p">
            {
                category.categoryTitle
                    ? <ListItem style={{color: category.color, margin: 0}}
                                component="h3">{category.categoryTitle}</ListItem>
                    : null
            }

            <ListItem component="div">

                <List component="nav" className="width100p noPadding">
                    {
                        category.links.map(l => (
                            <ListItemLink key={l.path} onSelect={onSelect} path={l.path}>{l.title}</ListItemLink>
                        ))
                    }
                </List>

            </ListItem>
        </div>

    )
}


export default function DrawerMenu({siteMap, viewerRole, onSelect}) {
    return (

        <List className="drawerList">
            {siteMap
                .filter(cat => cat.roles.includes(viewerRole))
                .map(cat => (<Category category={cat} key={cat.categoryTitle} onSelect={onSelect}/>))
            }
        </List>
    )
}
