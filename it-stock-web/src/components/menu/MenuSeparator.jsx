import React from 'react';

export default function MenuSeparator() {
    return (
        <div style={{flexGrow: 1}}>&nbsp;</div>
    )
}
