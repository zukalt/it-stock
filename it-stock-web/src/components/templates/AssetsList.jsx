import React from 'react' ;

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import TextField from "@material-ui/core/TextField";
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';


const AdminAssetsListItem = ({cat, onEdit, onNew, onDelete}) => {

    return (
        <React.Fragment>
            <ListItem disableGutters>
                <ListItemText primary={cat.name} secondary={`${cat.items.length} item(s)`}/>
                <Button startIcon={<AddIcon/>} onClick={() => onNew({category: cat.name})}>{cat.name}</Button>
            </ListItem>
            <List disablePadding dense>
                {
                    cat.items.map(item => (
                        <ListItem key={item.id} style={{borderBottom: "1px dashed #e7e7e7", paddingRight: 0}}>
                            <ListItemText primary={item.name}/>
                            <IconButton onClick={() => onEdit({...item, category: cat.name})}><EditIcon/></IconButton>
                            <IconButton onClick={() => onDelete({...item, category: cat.name})}><DeleteIcon
                                color="secondary"/></IconButton>
                        </ListItem>
                    ))
                }

            </List>
        </React.Fragment>
    )
};

export default function AssetsList({assetTypes, onEdit, onNew, onDelete, onFilterChange}) {

    return (
        <List component="div">
            <ListItem disableGutters>
                <TextField label="Search..." fullWidth
                           onChange={event => onFilterChange(event.target.value)}/>
            </ListItem>
            {
                assetTypes.length === 0
                    ?
                    <ListItem>
                        <ListItemText>Nothing found</ListItemText>
                    </ListItem>
                    : assetTypes.map(cat => (
                        <AdminAssetsListItem key={cat.name} cat={cat} onEdit={onEdit} onNew={onNew} onDelete={onDelete} />
                    ))
            }
            <ListItem disableGutters>
                <Button fullWidth color="primary" startIcon={<AddIcon/>} onClick={() => onNew({})}>
                    New
                </Button>
            </ListItem>
        </List>
    )
}
