import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';


const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));


export default function SignIn({ errors, submitAction, defaultEmail, rememberDefault }) {
    const classes = useStyles();

    const handleSubmit = ($event) => {
        $event.preventDefault();
        // submitAction
        const email = $event.target.email.value;
        const password = $event.target.password.value;
        const remember = $event.target.remember.checked;
        submitAction && submitAction({email, password, remember});
    };
    

    return (
        <Container component="main" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">Sign in</Typography>
                {
                    errors && errors.length ?
                        errors.map(e => (
                            <Typography key={e} component='div' color="error" variant="subtitle1">{e}</Typography>
                        ))
                        :
                        <Typography component='div' variant="subtitle1">&nbsp;</Typography>
                }
                <form className={classes.form}  onSubmit={handleSubmit}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        label="Email Address"
                        name="email"
                        type="email"
                        autoComplete="email"
                        defaultValue={defaultEmail}
                        autoFocus
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                    />
                    <FormControlLabel
                        control={<Checkbox value="remember" name="remember" color="primary" defaultChecked={rememberDefault} />}
                        label="Remember me"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >Sign In</Button>
                </form>
            </div>
        </Container>
    );
}
