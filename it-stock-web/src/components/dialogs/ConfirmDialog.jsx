import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function ConfirmDialog({onClose,
                                          title = 'Please confirm',
                                          message = 'Are you sure?',
                                          btnYesText = 'Yes',
                                          btnNoText = 'No', ...rest
}) {

    const no = () => onClose(false);
    const yes = () => onClose(true);

    return (
        <Dialog open onClose={no} {...rest}>
            <DialogTitle>{title}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {message}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={no}>
                    {btnNoText}
                </Button>
                <Button color="primary" onClick={yes}>
                    {btnYesText}
                </Button>
            </DialogActions>
        </Dialog>
    );
}
