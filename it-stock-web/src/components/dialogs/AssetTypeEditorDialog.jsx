import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

function ifNotEmpty(value, msg) {
    return value ? null : msg;
}
function validateName(name) {
    return ifNotEmpty(name, "Name can't be blank")
}

function validateCategory(category) {
    return ifNotEmpty(category, "Category can't be blank")
}

function validate({name, category}) {
    return {
        "name": validateName(name),
        "category": validateCategory(category)
    }
}
function hasError(validation) {
    return Object.keys(validation).some(key => validation[key] != null)
}
export default function AssetTypeEditorDialog({item, onSave, onCancel, externalErrors}) {
    const {id, name, category} = item;
    const [validation, setValidation] = useState({name: null, category: null});

    const onSubmit = event => {
        event.preventDefault();
        const name = event.target.name.value;
        const category = event.target.category.value;
        const validation = validate({name, category}) ;

        if (hasError(validation)) {
            setValidation(validation)
        }
        else {
            onSave({id, name, category})
        }
    };


    return (

        <Dialog open onClose={onCancel}>
            <DialogTitle>{id ? "Edit" : "New"}</DialogTitle>
            <form onSubmit={onSubmit} noValidate>
                <DialogContent>
                    <DialogContentText color="error">
                        {externalErrors}
                    </DialogContentText>
                    <TextField
                        disabled={Boolean(category)}
                        margin="dense"
                        label="Category"
                        type="text"
                        defaultValue={category}
                        name="category"
                        error={Boolean(validation.category)}
                        helperText={validation.category}

                        fullWidth
                    />
                    <TextField
                        margin="dense"
                        label="Name"
                        type="text"
                        defaultValue={name}
                        name="name"
                        error={Boolean(validation.name)}
                        helperText={validation.name}

                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={onCancel} color="primary">
                        Cancel
                    </Button>
                    <Button color="primary" type="submit">
                        Save
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    );
}
