import React from 'react';
import {
    AppTitle,
    AppBarComponent,
    AppBarMenuButton,
    UserMenu,
    MenuSeparator,
    DrawerMenu
} from '../menu';
import {Drawer, Button} from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';
import siteMap from './navigationData'
import {useHistory} from "react-router-dom";
import SyncMonitor from '../../features/sync/SyncMonitor'

import { useDispatch, useSelector} from "react-redux";
import {logOut} from '../../features/auth/authSlice'


const MenuWithDrawer = () => {
    const [drawerOpened, setDrawerState] = React.useState(false);
    const role = useSelector(state => state.auth.user.role);
    const history = useHistory();

    const openDrawer = () => setDrawerState(true);
    const closeDrawer = () => setDrawerState(false);
    const navigateAndClose = path => {
        history.push(path);
        closeDrawer();
    };

    return (
        <React.Fragment>
            <AppBarMenuButton visible={true} openMenu={openDrawer} />
            <Drawer anchor="left" open={drawerOpened} onClose={closeDrawer}>
                <Button onClick={closeDrawer} variant="text" className="width100p"><CloseIcon
                    fontSize="large"/></Button>
                <DrawerMenu siteMap={siteMap} viewerRole={role} onSelect={navigateAndClose}/>
            </Drawer>
        </React.Fragment>
    )
};

const BoundUserMenu = () => {

    const username = useSelector(state => state.auth.user.fullName);
    const dispatch = useDispatch();

    return (
        <UserMenu userName={username} logoutHandler={() => dispatch(logOut())}/>
    )
};

export default function AppBar() {
    return (
        <AppBarComponent>
            <MenuWithDrawer/>
            <AppTitle/>
            <MenuSeparator/>
            <SyncMonitor/>
            <BoundUserMenu/>
        </AppBarComponent>
    );
}

