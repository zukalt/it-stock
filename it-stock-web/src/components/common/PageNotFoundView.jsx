import React from 'react' ;
import {Typography, Button} from "@material-ui/core";
import NotListedLocationIcon from '@material-ui/icons/NotListedLocation';
import Container from "@material-ui/core/Container";
import {useHistory} from 'react-router-dom';


export default () => {
    const history = useHistory();

    return (
        <Container style={{padding: 80, textAlign: "center"}} maxWidth="md">
            <NotListedLocationIcon color="error" style={{fontSize: 80}}/>
            <Typography variant="h2">How did you get here?</Typography>

            <Button onClick={() => history.replace('/')}
                    style={{width: 200, margin: 20}}
                    variant="outlined" color="secondary">Start from Home</Button>
        </Container>

    )
}
