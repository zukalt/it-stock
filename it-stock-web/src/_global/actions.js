const FETCHING = 'FETCHING';
const FETCHED = 'FETCHED';


export default {
    FETCHING, FETCHED
}
