import axios from 'axios'

const client = axios.create({
    baseURL: `${process.env.PUBLIC_URL || ''}/api`,
    timeout: 1000,

});

function headers(token) {

    return token ? {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    } : {}
}

function http(method, url, data, params) {
    return {
        method,
        url,
        params,
        data
    }
}

function call(request, session) {
    return client.request({...request, ...headers(session)});
}

const API = {
    auth: {
        login: (email, password) => http('POST', '/auth/login', {email, password}),
        logout: () => http('POST', '/auth/logout', {}),
        whoami: () => http('GET', '/auth/whoami')
    },

    owners: {
        all: () => http('GET', '/owners', null, {first: 10000}),
        add: (entity) => http('POST', '/owners', entity),
        update: (entity) => http('PATCH', `/owners/${entity.id}`, entity),
        remove: id => http('DELETE', `/owners/${id}`)
    },

    assets: {
        all: () => http('GET', '/assets', null, {first: 10000}),
        add: (entity) => http('POST', '/assets', entity),
        update: (entity) => http('PATCH', `/assets/${entity.id}`, entity),
        remove: id => http('DELETE', `/assets/${id}`)
    },

    transfers: {
        history: ({ownerId, assetTypeId}) => http('GET', '/transfers', null, {ownerId, assetTypeId}),
        add: entity => http('POST', '/transfers', entity),
        undo: transferId => http('DELETE', `/transfers/${transferId}`)
    }
};

export function logIn(email, password) {
    return call(API.auth.login(email, password)).then(r => r.data)
}

export function logOut(token) {
    try {
        return call(API.auth.logout(), token)
    } catch (e) {
    }
}

export function fetchAssetTypes(token) {
    return call(API.assets.all(), token).then(r => ({assetTypes: r.data, version: Date.now()}));
}

export async function execute(task, token) {
    return call(task, token).then(r => r.data);
}

export default API;
