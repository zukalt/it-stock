import sha1 from 'js-sha1';

export default function generateId(...parts) {
    const hash = sha1.create();
    parts.forEach(part => hash.update(part))
    return hash.hex();
}
