import {createSlice} from '@reduxjs/toolkit'
import generateId from '../../_global/generateId'
import API, {execute} from '../../_global/api'
import {scheduleTask} from "../sync/syncSlice";

const assetTypesInitialState = {
    version: 0,
    all: []
};

const assetTypes = createSlice({
    name: 'assetTypes',
    initialState: assetTypesInitialState,
    reducers: {
        add(state, {payload}) {
            let {id, name, category} = payload;
            id = id || generateId(category, name);
            state.all.push({id, name, category});
        },
        update(state, {payload}) {
            const {id, name, category} = payload;
            state.all = state.all.map(t => t.id === id ? {id, category, name} : t)
        },
        remove(state, {payload}) {
            const id = payload.id || payload.toString();
            state.all = state.all.filter(t => t.id !== id)
        },
        reset(state, {payload}) {
            state.all = payload;
            state.version = Date.now();
        }
    }
});
export const {add, update, remove, reset} = assetTypes.actions;

export const addThenSync = newAssetType => dispatch => {

    newAssetType.id = generateId(newAssetType.category, newAssetType.name);
    dispatch(add(newAssetType)) ;
    const task = {
        id: newAssetType.id,
        job: API.assets.add(newAssetType),
        title: `Add ${newAssetType.category}/${newAssetType.name}`
    };
    dispatch(scheduleTask(task));

};

export const updateThenSync = assetType => dispatch => {
    dispatch(update(assetType)) ;
    const task = {
        id: assetType.id,
        job: API.assets.update(assetType),
        title: `Update ${assetType.category}/${assetType.name}`
    };
    dispatch(scheduleTask(task));
};

export const deleteThenSync = assetType => dispatch => {
    dispatch(remove(assetType)) ;
    const task = {
        id: assetType.id,
        job: API.assets.remove(assetType.id),
        title: `Deleting ${assetType.category}/${assetType.name}`
    };
    dispatch(scheduleTask(task));
};

export const fetch = () => (dispatch, getState) => {
    execute(API.assets.all(), getState().auth.token).then(r => dispatch(reset(r)));
};

export default assetTypes.reducer

