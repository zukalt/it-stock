import React, {useMemo, useState, Fragment, useCallback} from 'react' ;

import AssetsList from '../../components/templates/AssetsList'
import AssetTypeEditorDialog from '../../components/dialogs/AssetTypeEditorDialog';
import ConfirmDeleteDialog from '../../components/dialogs/ConfirmDialog';
import {useDispatch, useSelector} from "react-redux";
import debounce from 'lodash.debounce';
import {addThenSync, deleteThenSync, updateThenSync} from './assetTypesSlice'

const toAssetTypeTree = (flatAssetTypes, filter) => {
    filter = filter.toLowerCase();
    const types = flatAssetTypes.filter(el => el.name.toLowerCase().includes(filter));

    const categoryMap = {};
    types.forEach(({id, category, name}) => {
        categoryMap[category] || (categoryMap[category] = {name: category, items: []});
        categoryMap[category].items.push({id, name})
    });
    const assetTypes = Object.keys(categoryMap).sort().map(cat => categoryMap[cat]);

    return assetTypes;
};

export default function AdminAssetsList() {

    const dispatch = useDispatch();
    const [filter, setFilter] = useState('');
    const [editingItem, setEditingItem] = useState(null);
    const [itemToDelete, setItemToDelete] = useState(null);
    const flatAssetTypes = useSelector(state => state.assetTypes.all);
    const externalErrors = null;

    const onFilterChange = useCallback(debounce(setFilter, 500), []);
    const assetTypesTree = useMemo(() => toAssetTypeTree(flatAssetTypes, filter), [flatAssetTypes, filter]);


    const onCancel = () => setEditingItem(null);
    const onSave = item => {
        setEditingItem(null);
        if (item.id) {
            dispatch(updateThenSync(item))
        } else {
            dispatch(addThenSync(item))
        }
    };

    const onDelete = item => {
        setItemToDelete(item)
    };
    const onDeleteConfirmed = confirmed => {
        if (confirmed) {
            dispatch(deleteThenSync(itemToDelete));
        }
        setItemToDelete(null);
    };

    return (
        <Fragment>
            <AssetsList assetTypes={assetTypesTree} onEdit={setEditingItem} onNew={setEditingItem} onDelete={onDelete}
                        onFilterChange={onFilterChange}/>
            {
                editingItem ?
                    <AssetTypeEditorDialog item={editingItem} onSave={onSave} onCancel={onCancel}
                                           externalErrors={externalErrors}/>
                    : null
            }
            {
                itemToDelete ?
                    <ConfirmDeleteDialog message={`Delete ${itemToDelete.category}/${itemToDelete.name}?`}
                                         onClose={onDeleteConfirmed}/>
                    : null

            }
        </Fragment>
    )
}
