import React from 'react' ;
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import {Container} from "@material-ui/core";
import {useHistory} from "react-router-dom";
import AdminAssetsList from "./AdminAssetsList";
import PageNotFoundView from "../../components/common/PageNotFoundView";


export default function AdminIndex({match}) {
    const {tab} = match.params;
    const history = useHistory();
    const navigate = (event, tab) => {
        history.push(tab)
    };

    return (
        <Container>
            <Tabs value={tab} onChange={navigate}>
                <Tab label="Asset Types" value="asset-types"/>
                <Tab label="Asset Owners" value="owners"/>
                <Tab label="Users" value="users" />
            </Tabs>
            <div>
            {
                tab === 'asset-types' ? <AdminAssetsList /> :
                    tab === 'owners' ? <h1>TODO: owners </h1> :
                    tab === 'users' ? <h1>TODO: users  </h1> : <PageNotFoundView />
            }
            </div>
        </Container>
    )
}
