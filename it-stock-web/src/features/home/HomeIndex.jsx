import React from 'react' ;

import Grid from '@material-ui/core/Grid';
import Container from "@material-ui/core/Container";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import Avatar from '@material-ui/core/Avatar';
import CardContent from "@material-ui/core/CardContent";
import {useHistory} from 'react-router-dom';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

import siteMap from '../../components/common/navigationData';
import {useSelector} from "react-redux";

function CategoryCard({category, onSelect}) {
    if (!category || !category.categoryTitle) {
        return null;
    }

    return (
        <Grid item xs={12} lg={4}>
            <Card>
                <CardHeader
                    avatar={
                        <Avatar style={{backgroundColor: category.color}}>
                            {category.categoryTitle.substr(0, 1)}
                        </Avatar>
                    }
                    title={category.categoryTitle}
                    subheader={category.categorySubtitle}
                />
                <CardContent>
                    <List component="nav" className="width100p noPadding">
                        {
                            category.links.map(l => (
                                <ListItem key={l.path}
                                          button
                                          component="div"
                                          onClick={() => onSelect(l.path)}>{l.title}</ListItem>
                            ))
                        }
                    </List>

                </CardContent>
            </Card>
        </Grid>

    )
}

export default function HomeIndex() {
    const viewerRole = useSelector(state => state.auth.user.role);
    const history = useHistory();
    const onSelect = path => history.push(path);

    return (
        <Container className="containerPadding">
            <Grid container spacing={5}>
                {siteMap
                    .filter(cat => cat.roles.includes(viewerRole))
                    .map(cat => (
                        <CategoryCard category={cat} key={cat.categoryTitle} onSelect={onSelect}/>
                    ))
                }
            </Grid>
        </Container>
    )
}
