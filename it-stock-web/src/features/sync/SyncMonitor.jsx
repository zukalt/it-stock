import React, {useEffect} from 'react'
// import LoopIcon from '@material-ui/icons/Loop';
import AirplanemodeActiveIcon from '@material-ui/icons/AirplanemodeActive';
import WifiIcon from '@material-ui/icons/Wifi';
import ErrorIcon from '@material-ui/icons/Error';
import {useDispatch, useSelector} from "react-redux";
import {resumeSync, pauseSync} from './syncSlice'

export default function SyncMonitor() {

    const sync = useSelector(state => state.sync);
    const {online,  error} = sync;
    const pending = sync.tasks.length;

    const dispatch = useDispatch();

    const goOnline = () => dispatch(resumeSync());
    const goOffline = () => dispatch(pauseSync());

    // eslint-disable-next-line
    useEffect(() =>  dispatch(resumeSync()), []);

    let icon = online ? <WifiIcon onClick={goOffline}/> : <AirplanemodeActiveIcon onClick={goOnline}/>;
    let text = pending === 0 ? 'Synced' : `Unsynced: ${pending}`;
    if (error) {
        icon = <ErrorIcon onClick={goOnline}/>;
        text = error.toString();
    }

    return (
        <span style={{fontSize: '70%', padding: 2}}>
            <span style={{textAlign: "top"}}>{text}</span> {icon}
        </span>
    )
}
