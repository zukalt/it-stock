import {createSlice} from '@reduxjs/toolkit'
import {execute} from "../../_global/api";
import {loggedOut} from '../auth/authSlice'

const syncInitialState = {
    error: null,
    online: false,
    tasks: []
};
let syncInProgresses = false;

const sync = createSlice({
    name: "sync",
    initialState: syncInitialState,
    reducers: {
        setOnline(state, {payload}) {
            state.online = payload;
            if (payload) {
                state.error = null;
            }
        },
        enqueue(state, {payload}) {
            state.tasks.push(payload);
        },
        dequeue(state) {
            state.tasks.shift();
            state.error = null;
        },
        syncFailed(state, {payload}) {
            state.online = false;
            state.error = payload;
        }
    }

});

export const executeApiCall = (apiCallFunc, onSuccess) => dispatch => {
    apiCallFunc()
        .then(
            response => onSuccess(response.data)
        )
        .catch(
            error => {
                const response = error.response;
                if (response) {// got server response
                    if ([401, 403].includes(response.status)) { // not authorized or session timed out
                        dispatch(loggedOut()); // force log out
                    } else {
                        dispatch(sync.actions.syncFailed(response.statusText));
                    }

                } else if (error.request) {
                    dispatch(sync.actions.syncFailed('Network not available'));
                } else {
                    dispatch(sync.actions.syncFailed('App Error :('));
                }
            }
        );
};

const syncLoop = () => (dispatch, getState) => {
    if (syncInProgresses) {
        return;
    }
    const nextTask = getState().sync.tasks[0];
    const token = getState().auth.token;
    if (!nextTask || !token) {
        syncInProgresses = false;
        return;
    }
    syncInProgresses = true;
    const apiCall = () => execute(nextTask.job, token);
    const apiCallThunk = executeApiCall (apiCall, function onTaskDone() {
        dispatch(sync.actions.dequeue());
        syncInProgresses = false;
        if (getState().sync.online) {
            dispatch(syncLoop());
        }
    });
    dispatch(apiCallThunk);
};
export const resumeSync = () => dispatch => {
    dispatch(sync.actions.setOnline(true));
    dispatch(syncLoop());
};
export const pauseSync = () => dispatch => {
    dispatch(sync.actions.setOnline(false)) ;
};

export const scheduleTask = task => (dispatch, getState) => {
    dispatch(sync.actions.enqueue(task));
    if (getState().sync.online) {
        dispatch(resumeSync());
    }
};

export const waitToComplete = (store, taskId) => {

    let sync  = store.getState().sync;

    const didComplete = (syncState) => {
        return !syncState.tasks.some(task=> task.id === taskId);
    };

    return new Promise(resolve => {
        if (didComplete(sync)) {
            return resolve(taskId);
        }
        const unsubscribe = store.subscribe(function () {
            if (sync !== store.getState().sync) {
                sync = store.getState().sync;
                if (didComplete(sync)) {
                    unsubscribe();
                    return resolve(taskId);
                }
            }
        })
    })
};


export default sync.reducer;
