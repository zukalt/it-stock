import { connect } from 'react-redux'
import LoginForm from '../../components/templates/LoginForm'
import { logIn } from './authSlice'

const mapStateToProps = state => ({
        defaultEmail: state.auth.user.email,
        rememberDefault: state.auth.remember,
        errors: state.auth.errors
    });
const actionCreators = {
    submitAction: logIn
};

export default connect(mapStateToProps, actionCreators)(LoginForm)	
