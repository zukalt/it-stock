import {createSlice} from '@reduxjs/toolkit'
import * as api from '../../_global/api'

const authInitialState = {
    "token": null,
    remember: true,
    "user": {
        email: null,
        fullName: null,
        role: null
    },
    errors: []
};

const auth = createSlice({
    name: 'auth',
    initialState: authInitialState,
    reducers: {
        logInStart(state, {payload}) {
            const {email, remember} = payload;
            state.remember = remember;
            state.user.email = email;
            state.errors = [];
        },
        loginError(state, {payload}) {
            state.errors.push(payload);
        },
        loggedIn(state, {payload}) {
            state.user = payload.user;
            state.token = payload.token;
        },
        loggedOut(state) {
            state.token = null;
        }
    }
});

export const loggedOut = auth.actions.loggedOut;

export const logOut = () => async (dispatch, getState) => {
    const token = getState().auth.token;
    dispatch(auth.actions.loggedOut());
    await api.logOut(token);
};

export const logIn = payload => async dispatch => {
    const {email, password, remember} = payload;

    dispatch(auth.actions.logInStart({email, remember}));

    try {
        let session = await api.logIn(email, password);
        dispatch(auth.actions.loggedIn(session));
    } catch (e) {
        let message = e.message ? e.message : e.toString();
        dispatch(auth.actions.loginError(message));
    }
};

export default auth.reducer

