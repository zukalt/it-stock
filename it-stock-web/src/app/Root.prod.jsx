import React from 'react'
import { Provider } from 'react-redux'
import CssBaseline from '@material-ui/core/CssBaseline';
import App from './App'
const Root = ({ store }) => (
    <Provider store={store}>
        <CssBaseline />
        <App />
    </Provider>
);

export default Root
