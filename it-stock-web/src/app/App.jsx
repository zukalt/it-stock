import React, {useEffect} from 'react';
import AppBar from '../components/common/AppBar'
import {Switch, Route, BrowserRouter as Router} from "react-router-dom";
import HomeIndex from '../features/home/HomeIndex';
import ReportsIndex from '../features/reports/ReportsIndex';
import TransfersIndex from '../features/transfers/TransfersIndex';
import AdminIndex from '../features/admin/AdminIndex';
import PageNotFoundView from '../components/common/PageNotFoundView';
import LoginPage from "../features/auth/LoginPage";
import {useDispatch, useSelector} from "react-redux";
import {fetch as fetchAllAssets} from "../features/admin/assetTypesSlice";

const App = () => {
    // const role = useSelector(state => state.auth.user.role);
    const authenticated = useSelector(state => state.auth.token != null);
    const dispatch = useDispatch();
    useEffect(() => {
        if (authenticated) {
            dispatch(fetchAllAssets())
        }
    }, [authenticated, dispatch]);

    if (!authenticated) {
        return <LoginPage/>
    }

    return (
        <Router>
            <AppBar/>
            <Switch>
                <Route path="/" exact component={HomeIndex}/>
                <Route path="/reports/" component={ReportsIndex}/>
                <Route path="/transfers/" component={TransfersIndex}/>
                <Route path="/manage/:tab?" component={AdminIndex}/>
                <Route component={PageNotFoundView}/>
            </Switch>
        </Router>
    );
};

export default App;
