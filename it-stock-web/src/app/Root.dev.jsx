import React from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import CssBaseline from '@material-ui/core/CssBaseline';
import App from './App'
import DevTools from '../components/tools/DevTools'


const Root = ({ store }) => (
    <Provider store={store}>
        <CssBaseline />
        <App />
        <DevTools />
    </Provider>
);

Root.propTypes = {
    store: PropTypes.object.isRequired,
};

export default Root;
