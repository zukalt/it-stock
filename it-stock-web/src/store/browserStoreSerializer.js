import debounce from 'lodash.debounce'


const preloadState = () => {
    const baseState = localStorage.getItem("stock.auth");
    try {
        if (baseState !== null) {
            return JSON.parse(baseState) ;
        }
    }
    catch (e) {
    }
};

const stateChanged = state => {

    const baseState = {auth: state.auth} ;
    try {
        if (state.auth.remember) {
            localStorage.setItem("stock.auth", JSON.stringify(baseState)) ;
        }
        else {
            localStorage.removeItem("stock.auth") ;
        }
    }
    catch (e) {
        // silent
        console.error(e);
    }

};

const subscribeToStore = store => {
    store.subscribe(debounce(
        () => stateChanged(store.getState())
        , 1000 // serialize once e
    ))
};

export {
    preloadState, subscribeToStore
}
