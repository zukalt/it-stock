import { combineReducers } from 'redux'
import ActionTypes from '../_global/actions'
import auth from '../features/auth/authSlice'
import sync from '../features/sync/syncSlice'
import assetTypes from '../features/admin/assetTypesSlice'

const fetching = (state = false, action) => {
    switch(action.type) {
        case ActionTypes.FETCHED: return false;
        case ActionTypes.FETCHING: return true;
        default:
            return state;
    }
};

const owners = (state = {version: 0, all: {}}, action) => {
    return state;
};

export default combineReducers({
    fetching,
    auth,
    sync,
    assetTypes,
    owners
})
