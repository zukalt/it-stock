import configureStore from './configureStore';
import * as browserStore from './browserStoreSerializer';

const store = configureStore(browserStore.preloadState());
browserStore.subscribeToStore(store);

export default store;
